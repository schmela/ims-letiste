CPP=g++
CPPFLAGS=-std=c++98 -pedantic -Wextra -g -lsimlib

all: letiste

letiste: letiste.cc
	$(CPP) letiste.cc $(CPPFLAGS) -o letiste

run: letiste
	./letiste

pack:
	tar -czf 03_xhrade08_xcerna01.tar.gz Makefile letiste.cc dokumentace.pdf
