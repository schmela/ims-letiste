//====================================================================//
//                      SHO LETISTE - PROJEKT IMS                     //
//====================================================================//

//  Autori: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
//          Tereza Cerna,    xcerna01@stud.fit.vutbr.cz

//====================================================================//

#include <iostream>
#include "simlib.h"
using namespace std;

const int delkaRoku        = 31536000;    // sekundy
const int dnuZimniSezony   = 182;         // dni 
const int dnuLetniSezony   = 183;         // dni
const int kapacitaLetadla  = 180;
const int delkaDne         = 86400;       // sekund
const int delkaHodiny      = 3600;        // sekund

// typy letu
const int PRAVIDELNY       = 1;
const int NEPRAVIDELNY     = 2;

// cestujici bez zavazadel/se zavazadly
const bool SE_ZAVAZADLEM   = true;         // cestujici ma velke zavazadlo
const bool BEZ_ZAVAZADLA   = false;        // cestujici nema velke zavazadlo

// let do schenghenu/mimo schengen
const bool SCHENGEN        = true;         // let v ramci schengenu
const bool MIMO_SCHENGEN   = false;        // let mimo schengen



// promenne na pomocne vypisy
int i = 1;

int prislo = 0;
int odletelo = 0;
double A;

int lidi = 0;
int letu = 0;

//====================================================================//

// na letisti je 8 prepazek na odbaveni cestujicich
Store prepazkyCheckInVsechny("Vsechny check-in prepazky ",6);
Store odbaveniZavazadel("Odbaveni zavazadel",2);
Store pasovaKontrola("Pasova kontrola",2);
Store bezpecnostniUlicka("Bezpecnostni ramy",2);
Store gate("Odletova brana",7);

Store skladZavazadel("skladZavazadel", kapacitaLetadla*100);
Store pasovaKontrolaPrilet("Pasova kontrola pro prilet",2);

Store mistaProLetadla("Mista pro letadla", 3);
Facility pristavaciDraha("Pristavaci draha");

Histogram hist_cestujiciOdlet("doba cestujiciho na letisti pri odletu",0,900,12);
Histogram hist_bezpecnost("doba cekani na kontrolu",0,120,15);
Histogram hist_bezpecnostPrubeh("doba cestujiciho na kontrole",0,30,20);

//====================================================================//
//                           PRILET CESTUJICICH                       //
//====================================================================//

class CestujiciOdchod : public Process {

	bool maZavazadlo;
	bool doSchengenu;

	public:
	CestujiciOdchod(bool pMaZavazadlo, bool pDoSchengenu) {
		maZavazadlo = pMaZavazadlo;
		doSchengenu = pDoSchengenu;
	}
	
	void Behavior() {
		
		// PASOVA KONTROLA
		if (doSchengenu) {
			// schengen
			/*
			Enter(pasovaKontrolaPrilet,1);
			Wait(Exponential(1));
			Leave(pasovaKontrolaPrilet,1);
			*/
		}
 		else {
			// mimo staty schengenu
			Enter(pasovaKontrolaPrilet,1);
			Wait(Exponential(20));
			Leave(pasovaKontrolaPrilet,1);
		} 
	
		if (maZavazadlo) {
			Enter(skladZavazadel, 1);
		}

	}
};

//====================================================================//
//                        GENEROVANI CESTUJICICH                      //
//====================================================================//


// cesta zavazadla z pasu do skladu
class zavazadlaDoSkladu : public Process {
	Store *zavazadlaOdlet;

	public:
	zavazadlaDoSkladu(Store *sklad) : zavazadlaOdlet(sklad){};

	void Behavior() {
		zavazadlaOdlet->SetCapacity(zavazadlaOdlet->Capacity()+1);
		Enter(*zavazadlaOdlet,1);
		Wait(Exponential(15));
		Leave(*zavazadlaOdlet,1);
	}
};

class Cestujici : public Process {

	Store *checkIn;
	Store *zavazadlaOdlet;
	Facility *terminal;
	bool doSchengenu;

	public:
	Cestujici(Store *pCheckIn, Facility *pTerminal, Store *pZavazadlaOdlet, bool pSchengen) {
		checkIn = pCheckIn;
		zavazadlaOdlet = pZavazadlaOdlet;
		terminal = pTerminal;
		doSchengenu = pSchengen;
	}

	void Behavior() {

		// DEBUG promenne
		int mojeI = i++;
		double casPrichodu = Time;		
		prislo++;

		//cout << Time-A << "\t\tnovy zakaznik " << mojeI << endl;

		// CHECK-IN
		Enter(*checkIn,1);
				
		int pom;
		if (pom = Uniform(0,100) < 1) {
			// jde si koupit listek
			Wait(Exponential(240));
		}
		else {
			if ( pom < 35 ) {    
				// jde se checknout
				Wait(Uniform(30,60));
			}
			else {
				// check-in na internetu
			} 
		}


		Leave(*checkIn, 1);

		
		// ODBAVENI ZAVAZADEL
		if (Uniform(0,100) < 36) {    
			Enter(odbaveniZavazadel,1);
			Wait(Exponential(45));
			(new zavazadlaDoSkladu(zavazadlaOdlet))->Activate();
			Leave(odbaveniZavazadel,1);
		}
 		else {		
			// nema zavazadlo
		} 
		

		
		// PASOVA KONTROLA
		if (doSchengenu) {
			// schengen
			/*
			Enter(pasovaKontrola,1);
			Wait(Exponential(10));
			Leave(pasovaKontrola,1);
			*/
		}
 		else {
			// mimo staty schengenu
			Enter(pasovaKontrola,1);
			Wait(Exponential(20));
			Leave(pasovaKontrola,1);
		} 


		// BEZPECNOSTNI KONTROLA

		// svlekani
		Wait(Exponential(30));

		double casCekaniBezpecnost = Time;
		Enter(bezpecnostniUlicka,1);
		hist_bezpecnost(Time - casCekaniBezpecnost);

		double casBezpecnost = Time;

		// doba samotne prohlidky
		Wait(Uniform(20,25));

		
		if ( Uniform(0,100) < 30 )
		{
				// zapipal -> znovu
				Wait(Uniform(20,25));
				if ( (Uniform(0,100) < 30) )
				{
					// zapipal -> manualni prohlidka
					Leave(bezpecnostniUlicka,1);
					Wait(Exponential(90));
				}	
				else
					Leave(bezpecnostniUlicka,1);
		}
		else
			Leave(bezpecnostniUlicka,1);


		hist_bezpecnostPrubeh(Time - casBezpecnost);

		// oblekani
		Wait(Exponential(30));
		

		// PROSTORY RESTAURACI A OBCHODU
		// CEKANI NA OTEVRENI TERMINALU
	
		//cout << Time-A << "\t\tODLET ceka na terminal " << mojeI  << endl;

		Seize(*terminal);
		Wait(0.005);
		Release(*terminal);

		Enter(gate,1);
		Wait(Exponential(5));
		Leave(gate,1);

		//cout << Time-A << "\t\tODLET je v letadle " << mojeI << endl; 
		odletelo++;
		hist_cestujiciOdlet(Time-casPrichodu);
	}
};



//====================================================================//
//                             ODLET LETADLA                          //
//====================================================================//

// uz ma zabranou nabiraci plochu
class odletLetadla : public Process {

	Store *zavazadlaOdlet;

	public:
	odletLetadla(Store *sklad): zavazadlaOdlet(sklad){};

	void Behavior() {

		// ceka se 40 minut a pak by se melo odlitat
		Wait(2400);

		// nabere vsechny zavazadla
		Enter(*zavazadlaOdlet,zavazadlaOdlet->Capacity());

		// napred si musi zabrat pristavaci/odletvou drahu, pak teprve vraci nabiraci misto (jinac by byl deadlock)
		Seize(pristavaciDraha);

		// vraceni nabiraciho mista
		Leave(mistaProLetadla,1);

		// 5 minut trva samotny odlet
		Wait(300);

		// vrati se pristavaci draha pro dalsi lety
		Release(pristavaciDraha);

		//cout << Time-A << "\tODLET letadlo odletelo" << endl;
		
	}
};



//====================================================================//
//                          ULOZENI ZAVAZADLA                         //
//====================================================================//

// preprava zavazadla z letadla do letiste
class ulozeniZavazadla : public Process {
	void Behavior() {
		Leave(skladZavazadel,1);
	}
};

//====================================================================//
//                            PRILET LETADLA                          //
//====================================================================//

class novyPrilet : public Process {

	public:
		novyPrilet(int typ): typLetu(typ) {};
		int typLetu;

	void Behavior() {

		// musi byt pririta, jinak by letadlo mohlo dlouho krouzit nad letistem
		Priority = 3;

		// napred si zabere misto a pak az pristavaci plochu (predchazeni deadlocku)
		Enter(mistaProLetadla,1);

		// zabrani pristavaci drahy jen pro sebe
		Seize(pristavaciDraha);

		// za 10 minut letadlo pristane aji staci dojet na vykladaci misto
		Wait(Exponential(600));

		// uvolni pristavaci drahu
		Release(pristavaciDraha);

		// pocet cestujicich vyzivajicich tento let
		int cestujicich = (typLetu == PRAVIDELNY)? kapacitaLetadla*Uniform(0.7,0.8) : Exponential(kapacitaLetadla);

		// generuje zda let prileti z schengenu(60%) ci mimo nej(40%)
		bool schengen = (Uniform(0,100) > 40)? SCHENGEN : MIMO_SCHENGEN;

		// generujou se tu cestujici bud se zavazadlem nebo bez
		for (int i=0; i < cestujicich; i++){
			if ( Uniform(0,100) < 35 ) {

				// preprava zavazadla na pas trva 10-15 minut
				(new ulozeniZavazadla)->Activate(Time + Uniform(600,900));

				// cestujicimu trva cesta z letadla:                          vystup z letadla + cesta do haly
				(new CestujiciOdchod(SE_ZAVAZADLEM,schengen))->Activate(Time + Exponential(i) + Exponential(180));
			}
			else {
				(new CestujiciOdchod(BEZ_ZAVAZADLA,schengen))->Activate(Time + Exponential(i) + Exponential(180));
			}
		}

		// cca 10 minut trva nez se vylozi letadlo
		Wait(Exponential(600));

		// po vylozeni jede letadlo pryc z vykladacich/nakladacich mist
		Leave(mistaProLetadla,1);

	}
};


//====================================================================//
//                               CASOVA OSA                           //
//====================================================================//

class CasovaOsa : public Process {

	public:
	Store *checkInStore; 
	Store *zavazadlaOdlet;
	Facility *terminal;

	CasovaOsa() {
		checkInStore = new Store("Check-in",2);		
		zavazadlaOdlet = new Store("Sklad zavazadel pro odlet",0);
		terminal = new Facility("Otevreni terminalu");
	}

	void Behavior() {

		A = Time;

		Enter(*checkInStore,2);
		Seize(*terminal);

		// kazdy let potrebuje 2 prepazky
		Enter(prepazkyCheckInVsechny,2); 
		
		// za 20 minut se otevrou prepazky
		Wait(1200);		
		Leave (*checkInStore,2);
		//cout << "------------prepazky otevreny\n";		

		// za 80 minut od otevreni se prepazky zavrou
		Wait (4800);
		Leave (prepazkyCheckInVsechny,2);		
		//cout << Time-A << "\tODLET ---prepazky uzavreny" << endl;
		
		// zabere si nastupni misto pred terminalem	
		Enter(mistaProLetadla, 1);
		(new odletLetadla(zavazadlaOdlet))->Activate();

		//otevre se terminal
		Release(*terminal);
		//cout << Time-A << "\tODLET ---Terminal otevren" << endl ;

		// a za 20 minut se zase zavre...
		Wait(1200);
		Priority = 2;
		Seize(*terminal);
		Priority = 0;
						
		//cout << Time-A << "\tODLET ---Terminal uzavren" << endl ;
	}
};

//====================================================================//
//                           GENEROVANI LETADEL                       //
//====================================================================//

class novyOdlet : public Process {

	public:
		novyOdlet(int typ): typLetu(typ) {};
		int typLetu;

	void Behavior() {
		
		//cout << Time << "\t\t" << "ODLET novy let " << ((typLetu == PRAVIDELNY)? "PRAVIDELNY" : "NEPRAVIDELNY") << endl; 

		// stvoreni casove osy, ridi veci okolo 1 letu
		CasovaOsa *osa = new (CasovaOsa);
		osa->Activate();

		// pocet cestujicich vyzivajicich tento let
		int pocetCestujicich = (typLetu == PRAVIDELNY)? kapacitaLetadla*Uniform(0.7,0.8) : (kapacitaLetadla-Uniform(0,5));
		//cout << "v letu je " << pocetCestujicich << " cestujicich" << endl; 

		if (typLetu == NEPRAVIDELNY)
		{
				lidi += pocetCestujicich;
				letu++;
		}

		// generuje zda let leti do schengenu(60%) ci mimo nej(40%)
		bool schengen = (Uniform(0,100) > 40)? SCHENGEN : MIMO_SCHENGEN;

		// stvori se cestujici podle typu letadla
		for (int i=0; i < pocetCestujicich; i++){			
			//int norm = Normal(3000,1000);
			int norm = Uniform(0,6000);
			if (norm < 0) norm = 0;
			(new Cestujici(osa->checkInStore,osa->terminal,osa->zavazadlaOdlet,schengen))->Activate(Time + norm);	
		}
	}
};

class Timeout : public Event {
		Process *Id;
	public:
		Timeout(Process *p, double dt): Id(p) {
			Activate(Time+dt);			
		}
		void Behavior() {			
			Id->Cancel();
			Cancel();	
		}
};

class letniSezonaOdlet : public Process {
	void Behavior() {
		Timeout *t = new Timeout(this, delkaDne*dnuLetniSezony);

		// generovani pro kazdy den sezony
		for (int i=0; i < dnuLetniSezony; i++){

			// pravidelne lety po cely rok: 1 - 3 denne
			for (int j=0; j < Uniform(1, 3); j++){
				(new novyOdlet(PRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));
			}

			
			// nepravidelne lety v letni sezone: 10 - 12 denne
			for (int j=0; j < Uniform(10, 12); j++){
				(new novyOdlet(NEPRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));

			}
			
			Wait(delkaDne);
		}
	}
};

class letniSezonaPrilet : public Process {
	void Behavior() {
		Timeout *t = new Timeout(this, delkaDne*dnuLetniSezony);

		// generovani pro kazdy den sezony
		for (int i=0; i < dnuLetniSezony; i++){
			
			// pravidelne lety po cely rok: 1 - 3 denne
			for (int j=0; j < Uniform(1, 3); j++){
				(new novyPrilet(PRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));
			}

			// nepravidelne lety v letni sezone: 10 - 12 denne
			for (int j=0; j < Uniform(10, 12); j++){
				(new novyPrilet(NEPRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));
			}
			Wait(delkaDne);
		}
	}
};

class zimniSezonaOdlet : public Process {
	void Behavior() {
		Timeout *t = new Timeout(this, delkaDne*dnuZimniSezony);

		// generovani pro kazdy den sezony
		for (int i=0; i < dnuZimniSezony; i++){

			// pravidelne lety po cely rok: 1 - 3 denne
			for (int j=0; j < Uniform(1, 3); j++){
				(new novyOdlet(PRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));
			}
			Wait(delkaDne);
		}
	}
};


class zimniSezonaPrilet : public Process {
	void Behavior() {
		Timeout *t = new Timeout(this, delkaDne*dnuZimniSezony);

		// generovani pro kazdy den sezony
		for (int i=0; i < dnuZimniSezony; i++){

			// pravidelne lety po cely rok: 1 - 3 denne
			for (int j=0; j < Uniform(1, 3); j++){
				(new novyPrilet(PRAVIDELNY))->Activate(Time + Uniform(1 + 4*delkaHodiny,delkaDne));
			}
			Wait(delkaDne);
		}
	}
};

class SezonniCasovac : public Process {
	void Behavior() {
		Enter(skladZavazadel,skladZavazadel.Capacity());

		while(1) {
			
			//Print("LETNI SEZONA - 10-15 letu denne\n");
			
			(new letniSezonaOdlet)->Activate();
			(new letniSezonaPrilet)->Activate();
			pasovaKontrola.SetCapacity(4);
			bezpecnostniUlicka.SetCapacity(3);
			Wait(dnuLetniSezony*delkaDne);
			
			//Print("ZIMNI SEZONA - 1-3 lety denne\n");
			(new zimniSezonaOdlet)->Activate();	
			(new zimniSezonaPrilet)->Activate();			
			pasovaKontrola.SetCapacity(2);
			bezpecnostniUlicka.SetCapacity(2);
			Wait(dnuZimniSezony*delkaDne);
		}
	}
};

//====================================================================//
//                          HLAVNI PROGRAM                            //
//====================================================================//

int main() {

	RandomSeed(time(NULL));
	Init(0, 365*delkaDne-1+3*delkaHodiny);   // +3h aby stihly odletet lety co vzniknou tesne pred pulnoci
	(new SezonniCasovac)->Activate();
	Run();

	
	hist_cestujiciOdlet.Output();
	hist_bezpecnost.Output();
	hist_bezpecnostPrubeh.Output();

	prepazkyCheckInVsechny.Output();
	odbaveniZavazadel.Output();
	pasovaKontrola.Output();
	bezpecnostniUlicka.Output();
	gate.Output();

	skladZavazadel.Output();
	pasovaKontrolaPrilet.Output();
	pristavaciDraha.Output();
	mistaProLetadla.Output();
}
